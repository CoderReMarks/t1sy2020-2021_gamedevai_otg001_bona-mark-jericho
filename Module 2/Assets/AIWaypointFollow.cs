﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIWaypointFollow : MonoBehaviour
{
    //   public GameObject[] waypoints;
    public UnityStandardAssets.Utility.WaypointCircuit circuit;
    public int currentWaypointIndex;
   // public Transform targetTrans;
    float currentSpeed;
    float additiveSpeed;
    float movementSpeed;
    float rotSpeed;
    float accuracy;
    void Awake()
    {
        currentSpeed = 0f;
        additiveSpeed = 1f;
        movementSpeed = 5f;
        rotSpeed = 4f;
        accuracy = 5f;
    }

    void Start()
    {
        //waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
        currentWaypointIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (circuit.Waypoints.Length == 0) return;
        GameObject currentWaypoint = circuit.Waypoints[currentWaypointIndex].gameObject;

        Vector3 lookAtGoal = new Vector3(currentWaypoint.transform.position.x,
                                        this.transform.position.y,
                                        currentWaypoint.transform.position.z);
        Vector3 dir = lookAtGoal - this.transform.position;
        if (dir.magnitude < 1.0f)
        {
            currentWaypointIndex++;
            if (currentWaypointIndex >= circuit.Waypoints.Length)
            {
                currentWaypointIndex = 0;
            }
        }

        
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * rotSpeed);
        this.transform.Translate(0, 0, movementSpeed * Time.deltaTime);
    }
}
