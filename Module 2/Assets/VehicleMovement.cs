﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{
    public Transform goal;
    public float speed;
    public float rotSpeed;
    public float acceleration;
    public float deceleration;
    public float minSpeed;
    public float maxSpeed;

    public float breakAngle;

    private void Awake()
    {
       /* speed = 0f;
        rotSpeed = 15f;

        acceleration = 5f;
        deceleration = 5f;
        minSpeed = 0f;
        maxSpeed = 20f;
        breakAngle = 20f;*/
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 lookAtGoal = new Vector3(goal.position.x, this.transform.position.y, goal.position.z);
        Vector3 direction = lookAtGoal - this.transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, 
                                                   Quaternion.LookRotation(direction),
                                                   Time.deltaTime * rotSpeed);
       
        if (Vector3.Angle(goal.forward,this.transform.forward) > breakAngle && speed > 0.01f) //decelerate if turning
        {
            speed = Mathf.Clamp(speed - (deceleration * Time.deltaTime), minSpeed, maxSpeed);
        }
        else if (Vector3.Angle(goal.forward, this.transform.forward) < breakAngle)
        {
            speed = Mathf.Clamp(speed + (acceleration * Time.deltaTime), minSpeed, maxSpeed);
        }
        
        this.transform.Translate(0, 0, speed );
    }
}
