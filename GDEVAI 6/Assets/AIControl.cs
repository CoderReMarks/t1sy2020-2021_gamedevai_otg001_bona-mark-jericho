﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class AIControl : MonoBehaviour
{
    GameObject[] goalLocation;
    NavMeshAgent agent;
    Animator anim;
    float speedMultiplier;
    float detectionRadius = 5f;
    float fleeRadius = 10f;
    // Start is called before the first frame update
    void Start()
    {
        /* goalLocation = GameObject.FindGameObjectsWithTag("goal");
         agent = this.GetComponent<NavMeshAgent>();
         anim = this.GetComponent<Animator>();

         agent.SetDestination(goalLocation[Random.Range(0, goalLocation.Length)].transform.position);
         anim.SetTrigger("isWalking");
         anim.SetFloat("wOffset", Random.Range(0.1f, 1f));

         speedMultiplier = Random.Range(0.1f, 1.5f);
         agent.speed = 2f * speedMultiplier;
         anim.SetFloat("speedMultiplier", speedMultiplier);*/

        goalLocation = GameObject.FindGameObjectsWithTag("goal");
        agent = this.GetComponent<NavMeshAgent>();
        anim = this.GetComponent<Animator>();
        agent.SetDestination(goalLocation[Random.Range(0, goalLocation.Length)].transform.position);
        anim.SetFloat("wOffset", Random.Range(0.1f, 1f));
        ResetAgent();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (agent.remainingDistance < 1)
        {
            ResetAgent();
            agent.SetDestination(goalLocation[Random.Range(0, goalLocation.Length)].transform.position);

        }
    }

    void ResetAgent()
    {
        speedMultiplier = Random.Range(0.1f, 1.5f);
        agent.speed = 2 * speedMultiplier;
        agent.angularSpeed = 120f;
        anim.SetFloat("speedMultiplier", speedMultiplier);
        anim.SetTrigger("isWalking");
        agent.ResetPath();
    }

    public void DetectNewObstacle(Vector3 location)
    {
        if (Vector3.Distance(location, this.transform.position) < detectionRadius)
        {
            Vector3 fleeDirection = (this.transform.position - location).normalized;
            Vector3 newGoal = this.transform.position + fleeDirection * fleeRadius;

            NavMeshPath path = new NavMeshPath();
            agent.CalculatePath(newGoal, path);

            if (path.status != NavMeshPathStatus.PathInvalid)
            {
                agent.SetDestination(path.corners[path.corners.Length - 1]);
                anim.SetTrigger("isRunning");
                agent.speed = 10f;
                agent.angularSpeed = 500f;
            }
        }
    }

    public void DetectObjectiveWayPoint(Vector3 location)
    {
        if (Vector3.Distance(location, this.transform.position) < detectionRadius)
        {

            NavMeshPath path = new NavMeshPath();
            agent.CalculatePath(location, path);

            if (path.status != NavMeshPathStatus.PathInvalid)
            {
                agent.SetDestination(path.corners[path.corners.Length - 1]);
                anim.SetTrigger("isRunning");
                agent.speed = 10f;
                agent.angularSpeed = 500f;
            }
        }
    }
}
