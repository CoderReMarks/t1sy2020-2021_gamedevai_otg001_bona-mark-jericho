﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour
{
    NavMeshAgent agent;

    public GameObject target;

    public WASDMovement playerMovement;

    Vector3 wanderTarget;

    public float sightRange;

   /* public delegate void StateWhenSpotted();
    public StateWhenSpotted stateWhenSpotted;*/

    public enum States {Seek, Flee, Pursue,Evade,Wander,Hide,CleverHide};
    public States stateDefault;
    public States stateWhenSpotted;

    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        playerMovement = target.GetComponent<WASDMovement>();

    }

    void Seek(Vector3 location) //run after target
    {
        agent.SetDestination(location);
    }

    void Flee(Vector3 location) //run from target
    {
        Vector3 fleeDirection = location - this.transform.position;
        agent.SetDestination(this.transform.position - fleeDirection);
    }

    void Pursue()// predicts where the target will go and chases after
    {
        Vector3 targetDirection = target.transform.position - this.transform.position;
        float lookAhead = targetDirection.magnitude / (agent.speed + playerMovement.currentSpeed);
        Seek(target.transform.position + target.transform.forward * lookAhead);
    }

    void Evade()
    {
        Vector3 targetDirection = target.transform.position - this.transform.position;
        float lookAhead = targetDirection.magnitude / (agent.speed + playerMovement.currentSpeed);
        Flee(target.transform.position + target.transform.forward * lookAhead);

    }

    
    void Wander()
    {
        float wanderRadius = 20;
        float wanderDistance = 10;
        float wanderJitter = 1;

        wanderTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter,
                                    0f,
                                    Random.Range(-1.0f, 1.0f) * wanderJitter);
        //Adding a new vector3 that is randomly jittering
        wanderTarget.Normalize();
        wanderTarget *= wanderRadius; //Makes wonder area bigger/smaller

        Vector3 targetLocal = wanderTarget + new Vector3(0f, 0f, wanderDistance);
        Vector3 targetWorld = this.gameObject.transform.InverseTransformVector(targetLocal); //converts local
        Seek(targetWorld);
    }

    void Hide()
    {
        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;

        int hidingSpotCount = World.Instance.GetHidingSpots().Length;
        
        for (int i = 0; i < hidingSpotCount; i++)
        {
            Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 hidePosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5f;

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);
            if (spotDistance < distance)
            {
                chosenSpot = hidePosition;
                distance = spotDistance;
            }
        }

        Seek(chosenSpot);
    }

    void CleverHide()
    {
        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;
        Vector3 chosenDir = Vector3.zero;
        GameObject chosenGameObject = World.Instance.GetHidingSpots()[0];

        int hidingSpotCount = World.Instance.GetHidingSpots().Length;

        for (int i = 0; i < hidingSpotCount; i++)
        {
            Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 hidePosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5f;

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);
            if (spotDistance < distance)
            {
                chosenSpot = hidePosition;
                chosenDir = hideDirection;
                chosenGameObject = World.Instance.GetHidingSpots()[i];
                distance = spotDistance;
            }
        }

        Collider hideCol = chosenGameObject.GetComponent<Collider>();
        Ray back = new Ray(chosenSpot, -chosenDir.normalized);
        RaycastHit info;
      
        hideCol.Raycast(back, out info, sightRange);
        Seek(info.point + chosenDir.normalized * 5f);
    }

    bool canSeeTarget()
    {
        RaycastHit raycastInfo;
        Vector3 rayToTarget = target.transform.position - this.transform.position;
   
        if (Physics.Raycast(this.transform.position,rayToTarget,out raycastInfo,sightRange))
        {
         
                // Debug.Log(this.gameObject.name + " - " + raycastInfo.transform.gameObject.name + " - " + raycastInfo.transform.gameObject.tag.ToString());
                return raycastInfo.transform.gameObject.tag == "Player";
            

        }
        return false;
    }

    // Update is called once per frame
    void Update()
    {
       
        if (canSeeTarget())
        {
       

            identifyState(stateWhenSpotted,target.transform);
        }
        else if (!canSeeTarget())
        {
            identifyState(stateDefault);
           
        }

    }
    //   public enum States { Seek, Flee, Pursue, Evade, Wander, Hide, CleverHide };
     void identifyState(States chosenState, Transform location = null)
    {
        if (chosenState == States.Wander)
        {
            Wander();
        }
        else if (chosenState == States.Seek)
        {
            Seek(location.position);
        }
        else if (chosenState == States.Flee)
        {
            Flee(location.position);
        }
        else if (chosenState == States.Pursue)
        {
            Pursue();
        }
        else if (chosenState == States.Evade)
        {
            Evade();
        }
        else if (chosenState == States.Hide)
        {
            Hide();
        }
        else if (chosenState == States.CleverHide)
        {
            CleverHide();
        }
    }
}
