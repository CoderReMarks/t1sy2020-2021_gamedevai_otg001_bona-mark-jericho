﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    Transform goal;
    float speed = 5.0f;
    float accuracy = 2.0f;
    float rotSpeed = 7.0f;
    public GameObject wpManager;
    GameObject[] wps;
    GameObject currentNode;
    int lastButton = -1;
    int currentWaypointIndex = 0;
    
    Graph graph;
    // Start is called before the first frame update
    void Start()
    {
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        graph = wpManager.GetComponent<WaypointManager>().graph;
        currentNode = wps[0];

    
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (graph.getPathLength() == 0 || currentWaypointIndex == graph.getPathLength()) //checks if graph has waypoints and is within bounds
        {
            return;
        }

        currentNode = graph.getPathPoint(currentWaypointIndex);

        if (Vector3.Distance(graph.getPathPoint(currentWaypointIndex).transform.position, transform.position) < accuracy)
        {
        
  
            currentWaypointIndex++;
 

        }

        //if tank is on its way 
        if (currentWaypointIndex < graph.getPathLength())
        {
            
            goal = graph.getPathPoint(currentWaypointIndex).transform;
            Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
                Vector3 direction = lookAtGoal - this.transform.position;
                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotSpeed);
                this.transform.Translate(0, 0, speed * Time.deltaTime);
        }

 
       
    }
    public void GoToHelipad()
    {
        if (lastButton != 0)
        {
            lastButton = 0;
            graph.AStar(currentNode, wps[0]);

            currentWaypointIndex = 0;
        }

    }

    public void GoToTwinMountains()
    {
        if (lastButton != 1)
        {
            lastButton = 1;
            graph.AStar(currentNode, wps[12]);

            currentWaypointIndex = 0;
        }
 
    }

    public void GoToRuins()
    {
        if (lastButton != 2)
        {
            lastButton = 2;
            graph.AStar(currentNode, wps[14]);
            currentWaypointIndex = 0;
        }
    }

    public void GoToFactory()
    {
        if (lastButton != 3)
        {
            lastButton = 3;

            graph.AStar(currentNode, wps[17]);
            currentWaypointIndex = 0;
        }
    }

    public void GoToBarracks()
    {
        if (lastButton != 4)
        {
            lastButton = 4;
            graph.AStar(currentNode, wps[20]);
            currentWaypointIndex = 0;
        }
    }
    //wait it's the same as command post?
    public void GoToCommandCenter()
    {
        if (lastButton != 5)
        {
            lastButton = 5;
            graph.AStar(currentNode, wps[4]);
            currentWaypointIndex = 0;
        }
    }

    public void GoToOilRefineryPumps()
    {
        if (lastButton != 6)
        {
            lastButton = 6;
            graph.AStar(currentNode, wps[15]);
            currentWaypointIndex = 0;
        }
    }

    public void GoToTankers()
    {
        if (lastButton != 7)
        {
            lastButton = 7;
            graph.AStar(currentNode, wps[16]);
            currentWaypointIndex = 0;
        }
    }

    public void GoToRadar()
    {
        if (lastButton != 8)
        {
            lastButton = 8;
            graph.AStar(currentNode, wps[13]);
            currentWaypointIndex = 0;
        }
    }
    //Command Post (the barracks behind the radar) 
    public void GoToCommandPost()
    {
        if (lastButton != 9)
        {
            lastButton = 9;
            graph.AStar(currentNode, wps[4]);
            currentWaypointIndex = 0;
        }
    }
    public void GoToMiddleOfMap()
    {
        if (lastButton != 10)
        {
            lastButton = 10;
            graph.AStar(currentNode, wps[19]);
            currentWaypointIndex = 0;
        }
    }
    /*
     * 
     * Twin Mountains
Barracks
Command Center
Oil Refinery Pumps
Tankers
Radar (the one with the huge circle on top of a rig)

Middle of the map*/
}
