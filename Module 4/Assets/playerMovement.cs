﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
    public CharacterController controller;
    //  public Rigidbody Character;


    float x;
    float z;

    public float speed;


    // Start is called before the first frame update
    void Start()
    {

    }

    void Update()
    {
        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");
    }

    void FixedUpdate()
    {
     


        Vector3 move = Vector3.ClampMagnitude(transform.right * x + transform.forward * z, 1.0f);
        controller.Move(move * speed * Time.fixedDeltaTime);


    }

}

