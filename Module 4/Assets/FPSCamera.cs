﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCamera : MonoBehaviour
{
    public float mouseSensitivity = 100f;

    public Transform TPCamera;
    public float xMin;
    public float xMax;
    float xRotation = 0f;


    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;//Prevents mouse from leaving the screen, mouse will always be in screen
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;



        //fps
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, xMax, xMin); //limit amount head can rotate
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        PlayerManager.instance.player.transform.Rotate(Vector3.up * mouseX);



    }
}
