﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    GameObject[] agents;
    float radiusDistanceFromPlayer = 5f;
    float radiusDistanceSeePlayer = 10f;
    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("AI");
    }

    // Update is called once per frame
    void Update()
    {
  
        foreach (GameObject ai in agents)
        {
           
            if (Vector3.Distance(PlayerManager.instance.player.transform.position, ai.transform.position) < radiusDistanceSeePlayer && Vector3.Distance(PlayerManager.instance.player.transform.position, ai.transform.position) > radiusDistanceFromPlayer)//if player is seen it'll follow
            {
                ai.GetComponent<AIController>().agent.SetDestination(PlayerManager.instance.player.transform.position);
            }
            else if (Vector3.Distance(PlayerManager.instance.player.transform.position, ai.transform.position) <= radiusDistanceFromPlayer) //If player is seen but needs space
            {
                ai.GetComponent<AIController>().agent.SetDestination(ai.transform.position);
            }
        }



        

         
    
        
    }
}
