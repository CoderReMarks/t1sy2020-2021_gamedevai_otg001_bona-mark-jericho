﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public bool alive;
    public float health;
    public float maxHealth;
    // Start is called before the first frame update
    void Start()
    {
        alive = true;
        health = maxHealth;

    }

    public void updateHealth(float healthModifier)
    {
        health += healthModifier;
        if (this.gameObject.CompareTag("Bot"))
        {
            this.gameObject.GetComponent<TankAI>().UIHealth.text = this.gameObject.name + ": " + health.ToString();
        }
        if (health > maxHealth)
        {
            health = maxHealth;
        }
        checkIfAlive();
    }

    void checkIfAlive()
    {
        if (health > 0)
        {
            alive = true;
        }
        else if (health <= 0)
        {
            alive = false;
            Destroy(this.gameObject);
        }

    }
}
