﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Flee : NPCBaseFSM
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateinfo, int layerindex)
    {
   
            npc.GetComponent<TankAI>().rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY;

            var direction = opponent.transform.position - npc.transform.position;

            float lookAhead = direction.magnitude / (npc.GetComponent<NavMeshAgent>().speed + opponent.GetComponent<Drive>().speed);
            var fleeDirection = (opponent.transform.position + opponent.transform.forward * lookAhead) - npc.transform.position;
            npc.GetComponent<NavMeshAgent>().SetDestination(npc.transform.position - fleeDirection);


    }


}
