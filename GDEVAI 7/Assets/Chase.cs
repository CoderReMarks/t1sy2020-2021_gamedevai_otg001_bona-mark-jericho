﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Chase : NPCBaseFSM
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
      
            Vector3 targetDirection = opponent.transform.position - npc.transform.position;
            float lookAhead = targetDirection.magnitude / (npc.GetComponent<NavMeshAgent>().speed + opponent.GetComponent<Drive>().speed);


        npc.GetComponent<NavMeshAgent>().SetDestination(opponent.transform.position + opponent.transform.forward * lookAhead);

    }
}
