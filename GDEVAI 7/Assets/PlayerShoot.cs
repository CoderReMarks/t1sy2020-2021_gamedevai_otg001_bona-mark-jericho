﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public GameObject bulletPrefab;
    public GameObject turret;

    [SerializeField] float timeLastShot;
    public float fireRate;
    public float bulletDamage;
    // Start is called before the first frame update
    void Start()
    {
        timeLastShot = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (timeLastShot < Time.time)
            {
                timeLastShot = Time.time + fireRate;
                shoot();
            }
           
        }
    }

    void shoot()
    {
        
        GameObject newBullet = Instantiate(bulletPrefab, turret.transform.position, turret.transform.rotation);
        newBullet.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
        newBullet.GetComponent<Bullet>().damage = bulletDamage;
    }
}
