﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TankAI : MonoBehaviour
{
    Animator anim;
    public GameObject player;
    public Rigidbody rb;
    public float bulletDamage;
    public float fireRate;
    public Text UIHealth;
    public GameObject GetPlayer()
    {
        return player;
    }

    public GameObject bullet;
    public GameObject turret;
    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
        fireRate = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
        anim.SetFloat("health", this.gameObject.GetComponent<Health>().health);
    }
    
    void Fire()
    {
        GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
        b.GetComponent<Bullet>().damage = bulletDamage;
    }

    public void StartFiring()
    {
        InvokeRepeating("Fire", 0.5f, fireRate);
    }

    public void StopFiring()
    {
        CancelInvoke("Fire");
    }
}
