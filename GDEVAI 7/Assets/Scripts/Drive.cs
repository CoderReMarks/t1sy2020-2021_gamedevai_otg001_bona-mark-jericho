﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drive : MonoBehaviour {

    public Rigidbody rb;
 	public float speed = 10.0F;
    public float rotationSpeed = 10.0f;
    public Vector3 move_Input;
    private void Update()
    {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        move_Input = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
        move_Input.Normalize();
    }

    void FixedUpdate() {

       
     //   this.gameObject.GetComponent<Rigidbody>().velocity = ;
        //transform.Translate(0, 0, translation);
     
     
        //   look_Input = new Vector3(look_direction.Horizontal, 0, look_direction.Vertical);
        // this.transform.eulerAngles(new Vector3(0, Mathf.Rad2Deg * Mathf.Atan2(look_direction.Vertical,look_direction.Horizontal), 0));  
        //  Debug.Log(look_Input);

        // rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePosition;
        /* if (look_Input != new Vector3(0f, 0f, 0f))
         {

             rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePosition;
             this.transform.forward = look_Input;

         }*/

        if (move_Input == new Vector3(0f, 0f, 0f))
        {
            rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePosition;
        }

        else if (move_Input != new Vector3(0f, 0f, 0f))
        {
            //  rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
            rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
            rb.MovePosition(this.transform.position + (move_Input * speed * Time.fixedDeltaTime));
        //    transform.rotation = Quaternion.LookRotation(move_Input * rotationSpeed);

            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(move_Input), rotationSpeed * Time.deltaTime);

        }

    }
}
