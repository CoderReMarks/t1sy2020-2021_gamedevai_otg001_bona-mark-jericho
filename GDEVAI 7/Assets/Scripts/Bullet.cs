﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject explosion;
    public float damage;
    private void OnTriggerEnter(Collider hit)
    {
        GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
        Destroy(e, 1.5f);
        if (hit.gameObject.transform.root.gameObject.CompareTag("Player") || hit.gameObject.transform.root.gameObject.CompareTag("Bot"))
        {
            hit.gameObject.transform.root.gameObject.GetComponent<Health>().updateHealth(-damage);

        }
    
            Destroy(this.gameObject);
    }


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
