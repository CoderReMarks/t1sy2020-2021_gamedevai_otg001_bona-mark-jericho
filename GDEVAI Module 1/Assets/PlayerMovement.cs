﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float movementSpeed;
    public float faceTurnSpeed;      
    // Start is called before the first frame update
    void Awake()
    {
        movementSpeed = 5f;
        faceTurnSpeed = 4f;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movementDir = new Vector3(Input.GetAxisRaw("Horizontal"),0f,Input.GetAxisRaw("Vertical"));

        movementDir.Normalize(); //fixes diagonal movement

        Vector3 mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);

        Vector3 faceDir = new Vector3(mousePos.x - transform.position.x, 0f, mousePos.z - transform.position.z);

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(faceDir), Time.deltaTime * faceTurnSpeed);

        if (movementDir != new Vector3(0f, 0f, 0f)) //if direction was pressed
        {
            this.transform.Translate(movementDir * movementSpeed * Time.deltaTime, Space.World);
            Camera.main.GetComponent<TopDownCamera>().UpdateCameraPosition(this.transform);
        }
      

       

   
    }
}
