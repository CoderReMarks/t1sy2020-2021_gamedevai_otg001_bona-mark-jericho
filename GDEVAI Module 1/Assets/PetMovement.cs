﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetMovement : MonoBehaviour
{

    public Transform targetTrans;
    [SerializeField]float currentSpeed;
    float additiveSpeed;
    float maxSpeed;
    float faceTurnSpeed;
    float radiusDistanceFromPlayer;
    // Start is called before the first frame update
    void Awake()
    {
        currentSpeed = 0f;
        additiveSpeed = 0.005f;
        maxSpeed = 0.75f;
        faceTurnSpeed = 4f;
        radiusDistanceFromPlayer = 3f;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPos = new Vector3(targetTrans.position.x, this.transform.position.y, targetTrans.position.z);
        Vector3 faceDir = targetPos - transform.position;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(faceDir), Time.deltaTime * faceTurnSpeed); //accelerate
        if (Vector3.Distance(targetPos, transform.position) > radiusDistanceFromPlayer + 2f)
        {
            //Accelerate Decellerate Lerp
            currentSpeed += additiveSpeed;
            currentSpeed = Mathf.Clamp(currentSpeed, 0, maxSpeed); //so it wont accelerate to much
        }
        else if (Vector3.Distance(targetPos, transform.position) <= radiusDistanceFromPlayer + 2f && Vector3.Distance(targetPos, transform.position) > radiusDistanceFromPlayer ) //decelerate
        {
            //Accelerate Decellerate Lerp`
            currentSpeed -= 0.005f;
            currentSpeed = Mathf.Clamp(currentSpeed, 0, maxSpeed); //just to make sure its 0
        }
        else if (Vector3.Distance(targetPos, transform.position) <= radiusDistanceFromPlayer) //just to make sure if reached radius around player, make sure it stop moving
        {
            currentSpeed = 0;
        }
        Vector3 newPosition = Vector3.Lerp(this.transform.position, targetTrans.position, Time.deltaTime * currentSpeed);
        this.transform.position = newPosition;
    }
}
