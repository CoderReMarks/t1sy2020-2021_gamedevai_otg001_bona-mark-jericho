﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.transform.rotation = Quaternion.Euler(90f, 0f, 0f);
        this.transform.position = PlayerManager.instance.player.transform.position + new Vector3(0f, 4f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void UpdateCameraPosition(Transform plr)
    {
        this.transform.position = plr.position + new Vector3(0f, 4f, 0f);
    }
}
